<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PersonalCabinetController extends Controller
{
    public function cabinet(Request $request){

        if($request->session()->has('user_id') && $request->session()->has('token')){
            
            return view('cabinet',array('api_url'=>env('API_URL') ));
        }
        else{
            return redirect('/');
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    

    public function home(Request $request)
    {
      
        if($request->session()->has('user_id') && $request->session()->has('token')){
            return redirect('/cabinet');
        }
        
        return view('home',array('api_url'=>env('API_URL')));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ApiRequestsController extends Controller
{
    public function registration(Request $request)
    {
        try {
            $client = new Client(['base_uri' => env('API_URL')]);

            $params=array(
                'json'=>[
                'user_name' => $request->user_name,
                'password'=>$request->password
                ]
            );
            
            $client = new Client();
            $url_api=env('API_URL').'/api/registration';

            $res = $client->post($url_api, $params);
            $status_code=$res->getStatusCode();
            $body= $res->getBody()->getContents();
            $json_decoded=json_decode($body);
    
            return array('status'=>$json_decoded);
        } catch (Exception $e) {
            return array('status'=>0);
        }
    }

    public function login(Request $request)
    {
        $params=array(
            'json'=>[
            'user_name' => $request->user_name,
            'password'=>$request->password
            ]
        );
        
        $client = new Client();
        $url_api=env('API_URL').'/api/login';
        
        $res = $client->post($url_api, $params);
        $status_code=$res->getStatusCode();
        $body= $res->getBody()->getContents();
        $json_decoded=json_decode($body);

        if ($status_code==200) {
            $request->session()->put('user_name', $request->user_name);
            $request->session()->put('user_id', $json_decoded[0]->id);
            $request->session()->put('token', $json_decoded[0]->token);
        }
    }

    public function create_note(Request $request)
    {
        if ($request->session()->has('user_id') && $request->session()->has('token')) {
            $user_id=$request->session()->get('user_id');
            $token=$request->session()->get('token');
            
            $params=array(
                'json'=>[
                'user_id' => $user_id,
                'token'=>$token,
                'note'=>$request->new_note,
                ]
            );
      
            
            $client = new Client();
            $url_api=env('API_URL').'/api/notes';
            //create new note with POST request to server api
            $res = $client->post($url_api, $params);
            $status_code=$res->getStatusCode();
            $body= $res->getBody()->getContents();
            $json_decoded=json_decode($body);
            
            return $json_decoded;
        }
    }

    public function get_all_notes(Request $request)
    {
        if ($request->session()->has('user_id') && $request->session()->has('token')) {
            $user_id=$request->session()->get('user_id');
            $token=$request->session()->get('token');
            $params=array(
                'json'=>[
                'user_id' => $user_id,
                'token'=>$token,
                ]
            );
      
            
            $client = new Client();
            $url_api=env('API_URL').'/api/notes';
            //sending GET request to server API
            $res = $client->get($url_api, $params);
            $status_code=$res->getStatusCode();
            $body= $res->getBody()->getContents();
            $json_decoded=json_decode($body);

            return $json_decoded[0];
        }
    }

    public function update_note(Request $request)
    {
        if ($request->session()->has('user_id') && $request->session()->has('token')) {
            $user_id=$request->session()->get('user_id');
            $token=$request->session()->get('token');
            $note_id=$request->note_id;
            $note_value=$request->note_value;
            
            $params=array(
                'json'=>[
                'user_id' => $user_id,
                'token'=>$token,
                'note_id'=>$note_id,
                'note_value'=>$note_value
                ]
            );
      
            
            $client = new Client();
            $url_api=env('API_URL').'/api/notes/'.$note_id;
            //sending PATCH request to server api
            $res = $client->patch($url_api, $params);
            $status_code=$res->getStatusCode();
            $body= $res->getBody()->getContents();
            $json_decoded=json_decode($body);

            return $json_decoded;
        }
    }

    public function delete_note(Request $request)
    {
        if ($request->session()->has('user_id') && $request->session()->has('token')) {
            $user_id=$request->session()->get('user_id');
            $token=$request->session()->get('token');
            $note_id=$request->note_id;
            $note_value=$request->note_value;
            
            $params=array(
                'json'=>[
                'user_id' => $user_id,
                'token'=>$token,
                'note_id'=>$note_id,
                ]
            );
      
            
            $client = new Client();
            $url_api=env('API_URL').'/api/notes/'.$note_id;
            //sending DELETE request to server api
            $res = $client->delete($url_api, $params);
            $status_code=$res->getStatusCode();
            $body= $res->getBody()->getContents();
            $json_decoded=json_decode($body);

            return $json_decoded;
        }
    }
}

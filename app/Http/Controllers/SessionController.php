<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function is_session(Request $request){
        if($request->session()->has('user_id') && $request->session()->has('token')){
            return array('status'=>1);
        }else{
            return array('status'=>0);
        }
    }

    public function logout(Request $request){
        if($request->session()->has('user_id') ){
            $request->session()->forget('user_id');
        }
        if($request->session()->has('token')){
            $request->session()->forget('token');
        }
        if($request->session()->has('user_name')){
            $request->session()->forget('user_name');
        }
        return redirect('/');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/cabinet', 'PersonalCabinetController@cabinet');

Route::post('/login', 'ApiRequestsController@login');
Route::post('/registration', 'ApiRequestsController@registration');

Route::post('/create_note', 'ApiRequestsController@create_note');
Route::post('/get_all_notes', 'ApiRequestsController@get_all_notes');
Route::post('/update_note', 'ApiRequestsController@update_note');
Route::post('/delete_note', 'ApiRequestsController@delete_note');

Route::get('/logout', 'SessionController@logout');





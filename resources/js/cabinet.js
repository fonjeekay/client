$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    get_all_notes();
    $("#add_new_note").click(function(e){
        e.preventDefault();
        add_new_note();
    });
});

var get_all_notes=function(){
    $("#all_notes").empty();
    $.ajax({
        method:'post',
        type:'json',
        url:'/get_all_notes',
        success:function(data){
            
            $.each(data,function (index,each_record){


                var each_row='<tr class="tr'+each_record['id']+'"><td><input class="input inp_note" data-id="'+each_record['id']+'" value="'+each_record['note']+'" placeholder="note" ></td><td><a  class="button button button is-dark is-outlined delete_note" data-id="'+each_record['id']+'"   data-tr="tr'+each_record['id']+'"   ><i class="fas fa-trash-alt"></i></a></td></tr>';
                
                
                $("#all_notes").append(each_row);
            });
            $(".inp_note").change(function(){
                var note_id=$(this).attr('data-id');
                var note_value=$(this).val();
                $.ajax({
                    method:'post',
                    type:'json',
                    url:'/update_note',
                    data:{
                        'note_id':note_id,
                        'note_value':note_value
                    },
                    success:function(data){
                    }
                });
            });
            $(".delete_note").click(function(e){
                var note_id=$(this).attr("data-id");
                $.ajax({
                    method:'post',
                    type:'json',
                    url:'/delete_note',
                    data:{
                        'note_id':note_id,
                    },
                    success:function(data){
                        $('.tr'+note_id).remove();
                        
                    },
                    error: function (xhr, status) {
                        alert('Internal Server Error');
                    },
                });
            });
        }
    });
    
};

var add_new_note=function(){
    var new_note=$("#new_note").val();
    $.ajax({
        method:'post',
        type:'json',
        url:'/create_note',
        data:{
            'new_note':new_note
        },
        success:function(data){
            get_all_notes();
            
        }
    });
    $("#new_note").val('');
};


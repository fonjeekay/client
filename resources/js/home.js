$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#registration_form").submit(function(e){
        e.preventDefault();

        var form = $("#registration_form");
        var data = getFormData(form);

        var password=data['password'];
        var repeat_password=data['repeat_password'];
        if(password==repeat_password){
            $.ajax({
                method:'post',
                type:'json',
                url:'/registration',
                data:data,
                success:function(data){
                    alert('Profile Created');
                },
                error: function (xhr, status) {
                    alert('User Already Exist Or Internal Server Error');
                },
            });
        }else{
            alert('Passwords Do Not Match');
        }

        

        

    });
    $("#login_form").submit(function(e){
        e.preventDefault();
        var form = $("#login_form");
        var data = getFormData(form);
        $.ajax({
            method:'post',
            type:'json',
            url:'/login',
            data:data,
            success:function(data){
                console.log(data);
                window.location.href = "/cabinet";
            },
            error: function (xhr, status) {
                alert('Server Error');
            },
        });
    });
});

function getFormData(form){
    var unindexed_array = form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}
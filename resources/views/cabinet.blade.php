<!DOCTYPE html>
<html>
<head>
    @include('cabinet_head')
</head>
<body>
    @include('nav')
    <div class="container">
        <div class="column is-large"></div>
        <div class="column is-large"></div>
    </div>
    <div class="container">
            <div class="columns is-gapless is-multiline is-mobile">
                <div class="column"></div>
                <div class="column ">

                    
                        <h2 class="subtitle is-2 has-text-centered">Personal Cabinet</h2>
                        <h4 class="subtitle is-4 has-text-centered" >{{ Session::get('user_name')}}</h4>
   
                        <table class="table is-full ">
                                <thead>
                                        <tr><td><input id="new_note" class="input" placeholder="just type something here"></td><td><a id="add_new_note" class="button button button is-dark is-outlined ">Add&nbsp;<i class="fas fa-plus-circle"></i></a></td></tr>
                                </thead>
                                  <tbody id="all_notes">
                                       

                                  </tbody>
                                  <tfoot>
                                  </tfoot>
                              </table>

                </div>
                <div class="column "><a class="button button is-dark is-outlined" href="/logout">Logout</a></div>
            </div>
        </div>
        <hr>
        @include('footer')
</body>

</html>

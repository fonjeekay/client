<!DOCTYPE html>
<html>

<head>
    @include('home_head')
</head>

<body>
    @include('nav')

    <div class="container">
        <div class="column is-large"></div>
        <div class="column is-large"></div>
    </div>

    <div class="container">
        <div class="columns">
            <div class="column"></div>
            <div class="column ">
                <form id="registration_form" autocomplete="off"  >
                    <h2 class="title is-2">Registration</h2>
                    <div class="field is-medium">
                        <label class="label">Username</label>
                        <div class="control">
                            <input name="user_name" class="input" type="text" placeholder="User Name" autocomplete="false" minlength="4" maxlength="25" required  >
                        </div>
                    </div>
                    <div class="field is-medium">
                        <label class="label">Password</label>
                        <div class="control">
                            <input name="password" class="input" type="password" placeholder="Password" autocomplete="false" minlength="4" maxlength="25" required >
                        </div>
                    </div>
                    <div class="field is-medium">
                        <label class="label">Repeat Password</label>
                        <div class="control">
                            <input name="repeat_password" class="input" type="password" placeholder="Repeat Password" autocomplete="false" minlength="4" maxlength="25" required >
                        </div>
                    </div>
                    <button type="submit" class="button is-dark is-outlined">Submit</button>
                </form>
            </div>
            <div class="column"></div>
        </div>
    </div>
    <hr>
    <div class="container">
        <div class="column is-large"></div>
        <div class="column is-large"></div>
    </div>


    <div class="container">
        <div class="columns">
            <div class="column"></div>
            <div class="column">
                <form id="login_form" action="/cabinet">
             
                    <h2 class="title is-2">Login</h2>
                    <div class="field is-medium">
                        <label class="label">Username</label>
                        <div class="control">
                            <input name="user_name" class="input" type="text" placeholder="User Name" minlength="4" maxlength="25" required>
                        </div>
                    </div>
                    <div class="field is-medium">
                        <label class="label">Password</label>
                        <div class="control">
                            <input name="password"  class="input" type="password" placeholder="Password" minlength="4" maxlength="25" required>
                        </div>
                    </div>

                    <button type="submit" class="button is-dark is-outlined">login</button>
                </form>
            </div>
            <div class="column"></div>
        </div>
    </div>
    <hr>
    <div class="container">
            <div class="column is-large"></div>
            <div class="column is-large"></div>
        </div>
        @include('footer')
</body>

</html>

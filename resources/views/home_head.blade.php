<title>A Test Project By Eugene Shashkov</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="api_url" content="{{ $api_url }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

<script src="/js/home.js"></script>

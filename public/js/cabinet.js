/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/cabinet.js":
/*!*********************************!*\
  !*** ./resources/js/cabinet.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  get_all_notes();
  $("#add_new_note").click(function (e) {
    e.preventDefault();
    add_new_note();
  });
});

var get_all_notes = function get_all_notes() {
  $("#all_notes").empty();
  $.ajax({
    method: 'post',
    type: 'json',
    url: '/get_all_notes',
    success: function success(data) {
      $.each(data, function (index, each_record) {
        var each_row = '<tr class="tr' + each_record['id'] + '"><td><input class="input inp_note" data-id="' + each_record['id'] + '" value="' + each_record['note'] + '" placeholder="note" ></td><td><a  class="button button button is-dark is-outlined delete_note" data-id="' + each_record['id'] + '"   data-tr="tr' + each_record['id'] + '"   ><i class="fas fa-trash-alt"></i></a></td></tr>';
        $("#all_notes").append(each_row);
      });
      $(".inp_note").change(function () {
        var note_id = $(this).attr('data-id');
        var note_value = $(this).val();
        $.ajax({
          method: 'post',
          type: 'json',
          url: '/update_note',
          data: {
            'note_id': note_id,
            'note_value': note_value
          },
          success: function success(data) {}
        });
      });
      $(".delete_note").click(function (e) {
        var note_id = $(this).attr("data-id");
        $.ajax({
          method: 'post',
          type: 'json',
          url: '/delete_note',
          data: {
            'note_id': note_id
          },
          success: function success(data) {
            $('.tr' + note_id).remove();
          },
          error: function error(xhr, status) {
            alert('Internal Server Error');
          }
        });
      });
    }
  });
};

var add_new_note = function add_new_note() {
  var new_note = $("#new_note").val();
  $.ajax({
    method: 'post',
    type: 'json',
    url: '/create_note',
    data: {
      'new_note': new_note
    },
    success: function success(data) {
      get_all_notes();
    }
  });
  $("#new_note").val('');
};

/***/ }),

/***/ 1:
/*!***************************************!*\
  !*** multi ./resources/js/cabinet.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/client/resources/js/cabinet.js */"./resources/js/cabinet.js");


/***/ })

/******/ });